const path = require("path");
const webpack = require("webpack");

const HtmlWebpackPlugin = require("html-webpack-plugin");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");

module.exports = {
    entry: {
        app: "./src/index.jsx",
    },

    output: {
        path: path.resolve(__dirname, "../public/dist"),
        publicPath: "/",
        filename: "bundle.js",
    },

    module: {
        rules: [
            // Javascript
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: ["babel-loader"],
            },

            // Styles
            {
                test: /\.(scss|css)$/,
                use: ["style-loader", "css-loader", "sass-loader"],
            },

            // Images
            {
                test: /\.(?:ico|gif|png|jpg|jpeg)$/i,
                type: "asset/resource",
            },

            // Fonts and SVGs
            {
                test: /\.(woff(2)?|eot|ttf|otf|svg|)$/,
                type: "asset/inline",
            },
        ],
    },

    resolve: {
        extensions: ["", ".js", ".jsx", ".scss"],
    },

    plugins: [
        new webpack.EnvironmentPlugin(["NODE_ENV"]),
        new HtmlWebpackPlugin({
            template: path.resolve(__dirname, "../public/index.html"),
        }),
        new CleanWebpackPlugin(),
    ],
};
