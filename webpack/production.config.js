const path = require("path");
const webpack = require("webpack");
const { merge } = require("webpack-merge");

const MiniCssExtractPlugin = require("mini-css-extract-plugin");

const baseConfig = require("./base.config");

module.exports = merge(baseConfig, {
    mode: "production",
    devtool: "source-map",

    output: {
        path: path.resolve(__dirname, "../public/dist"),
        publicPath: "./",
        filename: "[name].bundle.[chunkhash].js",
    },

    module: {
        rules: [
            {
                test: /\.(scss|css)$/,
                use: [MiniCssExtractPlugin.loader, "css-loader", "sass-loader"],
            },
        ],
    },

    plugins: [
        // Minify CSS
        new MiniCssExtractPlugin({
            filename: "styles/[name].[contenthash].css",
            chunkFilename: "[id].css",
        }),
        new webpack.LoaderOptionsPlugin({
            minimize: true,
        }),
    ],
});
