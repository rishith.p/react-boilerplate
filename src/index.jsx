import React from "react";
import {render} from "react-dom";

import MyComponent from './components';

import "normalize.css";
import "./styles/index.scss";

render(<MyComponent/>, document.getElementById("root"));

module?.hot?.accept();
