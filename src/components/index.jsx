import React from 'react';

import './styles.scss';

const MyComponent = () => {
    return <div className="container">
        <div className="source">
            <span className="drag-content" draggable>Drag me down</span>
        </div>
        <div className="target"></div>
    </div>
}

export default MyComponent;